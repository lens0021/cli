module gitlab.wikimedia.org/releng/cli

require (
	github.com/AlecAivazis/survey/v2 v2.3.2
	github.com/Microsoft/go-winio v0.5.0 // indirect
	github.com/alecthomas/assert v0.0.0-20170929043011-405dbfeb8e38
	github.com/alecthomas/chroma v0.9.2 // indirect
	github.com/alecthomas/colour v0.1.0 // indirect
	github.com/alecthomas/repr v0.0.0-20210801044451-80ca428c5142 // indirect
	github.com/blang/semver v3.5.1+incompatible
	github.com/briandowns/spinner v1.16.0
	github.com/containerd/containerd v1.5.7 // indirect
	github.com/docker/docker v20.10.9+incompatible
	github.com/docker/go-connections v0.4.0 // indirect
	github.com/fatih/color v1.13.0
	github.com/google/go-cmp v0.5.6 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/hashicorp/go-retryablehttp v0.7.0 // indirect
	github.com/joho/godotenv v1.4.0
	github.com/mattn/go-colorable v0.1.11 // indirect
	github.com/microcosm-cc/bluemonday v1.0.15 // indirect
	github.com/moby/term v0.0.0-20201216013528-df9cb8a40635 // indirect
	github.com/morikuni/aec v1.0.0 // indirect
	github.com/muesli/reflow v0.3.0 // indirect
	github.com/profclems/glab v1.21.1
	github.com/rhysd/go-github-selfupdate v1.2.3
	github.com/rivo/tview v0.0.0-20211001102648-5508f4b00266 // indirect
	github.com/rodaine/table v1.0.1
	github.com/sergi/go-diff v1.2.0 // indirect
	github.com/spf13/cobra v1.2.1
	github.com/txn2/txeh v1.3.0
	github.com/ulikunitz/xz v0.5.10 // indirect
	github.com/xanzy/go-gitlab v0.51.1
	github.com/yuin/goldmark v1.4.1 // indirect
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	golang.org/x/net v0.0.0-20211014222326-fd004c51d1d6 // indirect
	golang.org/x/oauth2 v0.0.0-20211005180243-6b3c2da341f1 // indirect
	golang.org/x/sys v0.0.0-20211013075003-97ac67df715c // indirect
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/time v0.0.0-20210723032227-1f47c861a9ac // indirect
	google.golang.org/genproto v0.0.0-20211015135405-485ec31e706e // indirect
	google.golang.org/grpc v1.41.0 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/ini.v1 v1.63.2
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)

go 1.16
